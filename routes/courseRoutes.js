/*
	TO be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.

	the Router() method,will us to contain our routes
*/

const express = require("express");
const router = express.Router();

const auth = require("../auth")
const { verify } = auth;
const { verifyAdmin } = auth;

///Import the Course model so we can manipulate it and add a new course document.
// const Course = require("../models/Course");
const coursesControllers = require("../controllers/coursesControllers");

//All routes to courses now has an endpoint prefaced with /courses
//endpoint - /courses/
router.get('/', verify,verifyAdmin, coursesControllers.getAllCourses);

//endpoint /courses/
router.post('/', verify,verifyAdmin,coursesControllers.addCourse);

// Get all active - courses - (regular, non-logged in)
router.get('/activeCourses', coursesControllers.getActiveCourses);

// // Activity (S36-S37)
// router.get('/getSingleCourse', coursesControllers.getCourseById);

// We can pass a data in a route without the use of request body by passing a small amount of data throgh the url with the use of route params
// /endpoint/:routeParams
// http://localhost:4000/courses/getSingleCourse/id
router.get('/getSingleCourse/:courseId', coursesControllers.getCourseById);

// update a single course
// Pass the id of the course we want to update via route params
// The update details will be passed via request body.
router.put("/updateCourse/:courseId", verify, verifyAdmin, coursesControllers.updateCourse);

// archive a signle course
// Pass the id of the course we want to update via route params
router.delete("/archiveCourse/:courseId", verify, verifyAdmin, coursesControllers.archiveCourse)

module.exports = router;