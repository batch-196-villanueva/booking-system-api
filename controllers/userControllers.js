/* 
    Naming convention for controllers is that it should be named after the model/documents it is concerned with.

    Controllers are functions which contain the actual business logic of our API and is triggered by a route.

    MVC - models, views, and controllers
*/

// To create a controller, we first add it to our module.exports
// So that we can import the controllers from our module

// import the User model in the controllers instead because this is where we are now going to use it
const User = require("../models/User");
const Course = require("../models/Course");
// import bcrypt
const bcrypt = require("bcrypt");

// bcrypt is a package which allows us to has our passwords to add a layer of security for our users' details
const auth = require("../auth");

// userController.registerUser
module.exports.registerUser = (req,res)=>{

	//console.log(req.body);
	

	//Using the Course model, we will use its constructor to create our Course document which will follow the schema of the model, and add methods for document creation.

    // Using bcrypt, we're going to hide the user's password underneath a layer of randomized characters. Salt rounds are the number of times we randomize the string/password hash

    // bcrypt.hashSync(<string>,<saltRounds>)
    const hashedPw = bcrypt.hashSync(req.body.password,10)
    
	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo

	})
	// console.log(newCourse)

	// newCourse is now an object which follows the courseSchema and with additional methods from our Course constructor
	// .save() method is added into our newCourse object. This will allow us to save the content of newCourse into our collection
	//equivalent to db.courses.insertOne()
	newUser.save()

	// .then() allows us to process the result of a previous function/method in its own anonymous function
	.then(result => res.send(result))

	// .catch() - catches the errors and allows us to prcess and send to client
	.catch(error => res.send("Invalid input!"))

}
// getUserDetails should only allow the LOGGED-in user to get his own details
module.exports.getUserDetails = (req,res)=>{
	// console.log(req.user)
	// console.log(req.body)
	// req.user.id will allow us to ensure that the LOGGED IN USER or the USER THAT PASSED THE TOKEN will be able to get HIS OWN details and ONLY his own
    User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

}

module.exports.loginUser = (req,res)=>{

	console.log(req.body)
    /*
        Steps for logging in our user:
        1. find the user by its email
        2. If we found the user, we will check his password if the password input and the hashed password in our db matches.
        3. If we don't find a user, we will send a message to the client
        4. If upon checking the found user's password is the same as our input password, we will generate a "key" for the user to have authorization to acess certain features in our app

    */
   User.findOne({email:req.body.email})
	.then(foundUser => {
		// foundUser is the parameter that contains the result of findOne
		// findOne() returns null if it is not able to match any document
		if(foundUser === null){
			res.send({message: "No User Found."})
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

			if(isPasswordCorrect){

				/*
					To be able to create a "key" or token that allows/authorizes our logged in user around our applicaton, we have to create our own module called auth.js.

					This module will create an encoded string which contains our users' details.

					The encoded string is what we call a JSONWebToken or JWT.

					This JWT can only be properly 


				*/
				// auth.createAccessToken(foundUser);

				// console.log("We will create a token for the use if the password is correct")

				return res.send({accessToken: auth.createAccessToken(foundUser)})

			} else {
				return res.send({message: "Incorrect Password"})
			}

		}

	})
}

// Activity (S36-S37)

// checks if email exists or not
module.exports.checkEmail = (req,res)=>{
    User.findOne({email:req.body.email})
	.then(result => {
		// findOne will return null if no match is found
		// Send false if email does not exist
		// Send true if email exists
		if(result === null){
			return res.send(false)
		} else {
			return res.send(true)
		}
	})
	.catch(error => res.send(error))

}

module.exports.enroll = async (req,res) => {
	// check the id of the user who will enroll?
	// console.log(req.user.id)

	// check the id of the course we want to enroll?
	// console.log(req.body.courseId)

	// Validate the user if they are an admin or not.
	// If the user is an admin, send a message to client and end the response
	// Else we will continue
	if(req.user.isAdmin){
		return res.send({message: "Action Forbidden."});
	}
	
	/*
		Enrollment will come in 2 steps

		First, find the user who is enrolling and update his enrollments subdocument array. We will push the courseId in the enrollments array.
		Second, find the course where we are enrolling and update its enrollees subdocument array. We will push the userId in the enrollees array.

		Since we will access 2 collections in one action, we will have to wait for the completion of action instead of letting Javascript continue line per line

		async and await - async keyboard is added to a function to make our function asynchronous. Which means that instead of JS regular behaviour of running each code line by line we will be able to wait for the result of a function.

		TO be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get a result before proceeding.
	*/

	// return a boolean to our isUserUpdated variable to determine the result of the query and if we were able to save the courseId into our user's enrollment subdocument array.
	// For adding subdocument is User
	let isUserUpdated = await User.findById(req.user.id).then(user => {
		// check if you found the user's document:
		// console.log(user)
		// Add the courseId in an object and push that object into the user's enrollments.
		// Because we have to follow the schema of the enrollments subdocument array:
		let newEnrollment = {
			courseId: req.body.courseId
		}
		// If-else statement to check if enrollment already exists
		if (user.enrollments.find(enrollment => enrollment.courseId === req.body.courseId)){
			return user.save().then(course => false).catch(err => "You're already enrolled in this course!");
		} else {
			// access the enrollments array from our user and push the new enrollment subdocument into the enrollments array.
			user.enrollments.push(newEnrollment)
			// We must save our user document and return the value of saving our document.
			// then return true IF we push the subdocument successfully
			// catch and return error message if otherwise.
			return user.save().then( user => true).catch(err => err.message)
		} 

	}).catch(err => res.send("Something went wrong :("))

	// For adding subdocument in Course
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		let enrollee = {
			userId: req.user.id
		}
		// If-else statement to check if enrollment already exists
		if (course.enrollees.find(enrollee => enrollee.userId === req.user.id)){
			return course.save().then(course => false).catch(err => "You're already enrolled in this course!");
		} else {
			course.enrollees.push(enrollee);
			return course.save().then(course => true).catch(err => err.message);
		}

	}).catch(err => res.send("Something went wrong :("))

	// Ensure that we were able to both update the user and course document to add our enrollment and enrolle respectively and send a message to the client to end the enrollment process:

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled successfully!"})
	} else {
		return res.send({message: "You're already enrolled in this course!"})
	}

}